Name:           gnome-icon-theme
Version:        3.12.0
Summary:        GNOME Desktop icon theme
License:        LGPLv3+
Release:        14
URL:            http://www.gnome.org
Source0:        http://download.gnome.org/sources/gnome-icon-theme/3.12/gnome-icon-theme-%{version}.tar.xz
BuildRequires:  gtk2 icon-naming-utils >= 0.8.7 intltool librsvg2
BuildRequires:  gcc gtk-update-icon-cache
Requires:       hicolor-icon-theme
Provides:       gnome-icon-theme-legacy = %{version}-%{release}
Obsoletes:      gnome-icon-theme-legacy < %{version}-%{release}
BuildArch:      noarch

%description
The GNOME Icon Theme package contains an assortment of non-scalable icons of different sizes and themes.

%package        devel
Summary:        Development files for gnome-icon-theme
Requires:       gnome-icon-theme = %{version}-%{release}

%description    devel
Development files for gnome-icon-theme.

%prep
%autosetup -n gnome-icon-theme-%{version}
%build
%configure --enable-icon-mapping
%install
%make_install
touch $RPM_BUILD_ROOT%{_datadir}/icons/gnome/icon-theme.cache
cp %{_datadir}/icon-naming-utils/legacy-icon-mapping.xml .
export INU_DATA_DIR=$PWD

(cd $RPM_BUILD_ROOT%{_datadir}/icons/gnome
size_list="8x8 16x16 22x22 24x24 32x32 48x48 256x256"
for size in $size_list; do
        cd $size || continue
        for dir in `find . -type d`; do
                context="`echo $dir | cut -c 3-`"
                if [ $context ]; then icon-name-mapping -c $context; fi
        done
        cd ..
done)
(cd $RPM_BUILD_ROOT%{_datadir}/icons/gnome
 install -d scalable/actions
 install -d scalable/apps
 install -d scalable/devices
 install -d scalable/emblems
 install -d scalable/mimetypes
 install -d scalable/places
 install -d scalable/status)
(cd $RPM_BUILD_ROOT%{_datadir}; echo "%%defattr(-,root,root)"
 find icons/gnome \( -name *-rtl.png -o -name *-ltr.png -o -type f \) -printf "%%%%{_datadir}/%%p\n"
 find icons/gnome -type l -printf "%%%%{_datadir}/%%p\n"
 find icons/gnome ! -name *-rtl.png -printf "%%%%{_datadir}/%%p\n"
 find icons/gnome ! -name *-ltr.png -printf "%%%%{_datadir}/%%p\n"
 find icons/gnome -type d -printf "%%%%dir %%%%{_datadir}/%%p\n"
) > files.txt
%transfiletriggerin -- %{_datadir}/icons/gnome
gtk-update-icon-cache -f %{_datadir}/icons/gnome &>/dev/null || :
%transfiletriggerpostun -- %{_datadir}/icons/gnome
gtk-update-icon-cache -f %{_datadir}/icons/gnome &>/dev/null || :

%files -f files.txt
%doc COPYING AUTHORS
%ghost %{_datadir}/icons/gnome/icon-theme.cache
%exclude %{_datadir}/icons/gnome/*/categories/gnome-control-center.png

%files devel
%{_datadir}/pkgconfig/gnome-icon-theme.pc

%changelog
* Tue Jan 11 2022 caodongxia <caodongxia@huawei.com> - 3.12.0-14
- add BuildRequires gtk-update-icon-cache 

* Tue May 18 2021 lin.zhang <lin.zhang@turbolinux.com.cn> - 3.12.0-13
- add BuildRequires gcc

* Wed Dec 11 2019 Ling Yang <lingyang2@huawei.com> - 3.12.0-12
- Package init
